﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MasterClass : MonoBehaviour {
	
	private Transform myTransform;	//Toma las coordenadas y guardalas

	[HideInInspector]
	public Renderer rend;		//Para cambiar de colores

	public float moveSpeed;	 		//cuantos lugares se mueve la pieza
	public bool hasMovedBefore= false;		//Si ya se movio
	public bool isSelected= false;
	public bool hasCasted= false; 	//Checar si se hizo el raycast y no hacerlo siempre

	public GameManager.Colors PieceColor; 
	public GameManager.Types PieceType;			//es blanco o negro (piezas)

	//public Vector2 origin;			//Guarda la posición global (original)

	// Use this for initialization
	void Start () {
		
		myTransform = transform;
		rend = GetComponent<Renderer> ();
		//origin = myTransform.position;	//Sirve para saber si la pieza se ha movido de su lugar de origen

	}
	
	// Update is called once per frame
	void Update () {
		if (isSelected && !hasCasted) {	//Si la pieza se escoge y todavía no ha hehco un raycast 
		
			switch (PieceType) {
				case GameManager.Types.Pawn:
				Debug.Log ("Did check");
					CheckPawnMove();
				break;

				case GameManager.Types.Tower:
					CheckTowerMove ();
					CheckRoque (GameManager.Types.King);
				break;

				case GameManager.Types.Knight:
					CheckKnightMove ();
				break;

				case GameManager.Types.Bishop:
					CheckBishopMove ();
				break;

				case GameManager.Types.Queen:
					CheckQueenMove ();

				break;

				case GameManager.Types.King:
					CheckKingMove ();
					CheckRoque (GameManager.Types.Tower);

				break;
				
			}
			hasCasted = true;	//Hice raycast, dejar de checar
			myTransform.localScale += GameManager.instance.selectedScale;
			rend.material.color = GameManager.instance.selectedColor;
		}
	}
	private void OnMouseDown() {
		//El color de mi pieza igual que el turno && no he seleccionada otra pieza
		if (GameManager.instance.Turn == PieceColor && !isSelected && GameManager.instance.PieceSelected == 0) {
			
			GameManager.instance.PieceSelected = PieceType;
			GameManager.instance.MovingPiece = this.gameObject;
			isSelected = true;

		} else {		//Diferente color o pieza ya esta seleccionada, entonces quitar la selección de la pieza anterior
			
			GameManager.instance.ClearSelection ();
		}
	}
		
	void CheckPawnMove(){
		Debug.Log ("Did check pawn");
		Vector2 pos = new Vector2 (myTransform.position.x, myTransform.position.y);	//Vector con la posición actual de la pieza
		Vector2 finalPos;		//Posición final del movimiento
		Vector2 finalPosA;		//Posicón final del ataque
		float newSpeed = moveSpeed;

		if (!hasMovedBefore) { //Si el peón no se ha movido se mueve dos espacios
			
			newSpeed = moveSpeed * 2;
		}

		if (PieceColor == GameManager.Colors.Black) {
			
			finalPos = pos + (Vector2.down * newSpeed * 2);	//Punto final para calcular Raycast
			finalPosA = Vector2.down;	//calclar punto final de raycast

		} else {	//Si es blanca
			
			finalPos = pos + (Vector2.up * newSpeed * 2);		//Lo mismo pero para las blancas y para arriba
			finalPosA = Vector2.up;
		}


		//Raycast para que salve to lo que toca(posición inicial, final y capas por checar)
		RaycastHit2D[] hitM = Physics2D.LinecastAll(pos, finalPos, 1 << LayerMask.NameToLayer("RaycastThis"));
		//Debug.DrawLine(pos, finalPos, Color.green);

		for (int i = 0; i < hitM.Length; i++) {

			if (hitM [i].collider != null) {		//Que tenga collider
			
				if (!hitM [i].collider.GetComponent<SpaceCheck> ().hasPiece) {		//Si la tile detectada por el Raycast NO tiene pieza

					hitM [i].collider.GetComponent<SpaceCheck> ().myState = SpaceCheck.SpaceState.Can_Move; //Cambiar tile a CanMove
					Debug.Log ("hit spaces");

				} else
					break;	//hay una pieza aqui pero no puedo tomarla, para Raycast
			}
		}

		for (int j = -1; j < 2; j += 2) {	//checar ataque
			RaycastHit2D hitA = Physics2D.Raycast (pos, finalPosA + new Vector2 (j, 0), moveSpeed * 2, 1 << LayerMask.NameToLayer ("RaycastThis"));

			if (hitA.collider != null) {
				
				if (hitA.collider.GetComponent<SpaceCheck> ().hasPiece && hitA.collider.GetComponent<SpaceCheck> ().pieceColor != PieceColor) {	// Checar si tiene pieza y si tiene enemigo... Si sí, state = canTake

					hitA.collider.GetComponent<SpaceCheck> ().myState = SpaceCheck.SpaceState.Can_Take; //Cambiar tile a canTake

				}
			}
		}
	}


	void CheckTowerMove(){
		Vector2 pos = new Vector2 (myTransform.position.x, myTransform.position.y);

		for (int i = -1; i < 2; i++) {
			
			for (int j = -1; j < 2; j++) {
				
				if ((Mathf.Abs(i)) != (Mathf.Abs(j))){
					
					RaycastHit2D[] hit= Physics2D.RaycastAll(pos, new Vector2 (i, j), moveSpeed * 2, 1 << LayerMask.NameToLayer ("RaycastThis"));

						for (int a=0; a< hit.Length; a++){
						
						if (hit [a].collider != null) {
							
							if (hit [a].collider.GetComponent<SpaceCheck> ().hasPiece && hit [a].collider.GetComponent<SpaceCheck> ().pieceColor != PieceColor) {

								hit [a].collider.GetComponent<SpaceCheck> ().myState = SpaceCheck.SpaceState.Can_Take;
								break;
							} else if (!hit [a].collider.GetComponent<SpaceCheck> ().hasPiece) {
								hit [a].collider.GetComponent<SpaceCheck> ().myState = SpaceCheck.SpaceState.Can_Move;
							} else
								break;
						}
					}
				}
			}
		}
	}

	void CheckQueenMove(){
		Vector2 pos = new Vector2 (myTransform.position.x, myTransform.position.y);

		for (int i = -1; i < 2; i++) {

			for (int j = -1; j < 2; j++) {

				RaycastHit2D[] hit= Physics2D.RaycastAll(pos, new Vector2 (i, j), moveSpeed * 2, 1 << LayerMask.NameToLayer ("RaycastThis"));

					for (int a=0; a< hit.Length; a++){

						if (hit [a].collider != null) {

							if (hit [a].collider.GetComponent<SpaceCheck> ().hasPiece && hit [a].collider.GetComponent<SpaceCheck> ().pieceColor != PieceColor) {

								hit [a].collider.GetComponent<SpaceCheck> ().myState = SpaceCheck.SpaceState.Can_Take;
								break;
							} else if (!hit [a].collider.GetComponent<SpaceCheck> ().hasPiece) {
								hit [a].collider.GetComponent<SpaceCheck> ().myState = SpaceCheck.SpaceState.Can_Move;
							} else
								break;
					}
				}
			}
		}	
	}
	void CheckKingMove(){
		Vector2 pos = new Vector2 (myTransform.position.x, myTransform.position.y);

		for (int i = -1; i < 2; i++) {

			for (int j = -1; j < 2; j++) {

				RaycastHit2D hit= Physics2D.Raycast(pos, new Vector2 (i, j), moveSpeed * 2, 1 << LayerMask.NameToLayer ("RaycastThis"));

					if (hit.collider != null) {

						if (hit.collider.GetComponent<SpaceCheck> ().hasPiece && hit.collider.GetComponent<SpaceCheck> ().pieceColor != PieceColor) {

							hit.collider.GetComponent<SpaceCheck> ().myState = SpaceCheck.SpaceState.Can_Take;
							break;
						} else if (!hit.collider.GetComponent<SpaceCheck> ().hasPiece) {
							hit.collider.GetComponent<SpaceCheck> ().myState = SpaceCheck.SpaceState.Can_Move;
					}
				}
			}
		}	
	}

	void CheckBishopMove(){
		Vector2 pos = new Vector2 (myTransform.position.x, myTransform.position.y);

		for (int i = -1; i < 2; i+=2) {

			for (int j = -1; j < 2; j+=2) {

				RaycastHit2D[] hit= Physics2D.RaycastAll(pos, new Vector2 (i, j), moveSpeed * 2, 1 << LayerMask.NameToLayer ("RaycastThis"));

				for (int a=0; a< hit.Length; a++){

					if (hit [a].collider != null) {

						if (hit [a].collider.GetComponent<SpaceCheck> ().hasPiece && hit [a].collider.GetComponent<SpaceCheck> ().pieceColor != PieceColor) {

							hit [a].collider.GetComponent<SpaceCheck> ().myState = SpaceCheck.SpaceState.Can_Take;
							break;
						} else if (!hit [a].collider.GetComponent<SpaceCheck> ().hasPiece) {
							hit [a].collider.GetComponent<SpaceCheck> ().myState = SpaceCheck.SpaceState.Can_Move;
						} else
							break;
					}
				}
			}
		}	
	}

	void CheckKnightMove() {
		Vector2 pos = new Vector2 (myTransform.position.x, myTransform.position.y); //Vector con la posición actual de la pieza
		Vector2 castPos;

		for (int i = -1; i < 2; i += 2) {
			for (int j = -1; j < 2; j += 2) {
				castPos = pos + (new Vector2 (i, j) * 2);

				RaycastHit2D hitA = Physics2D.Raycast (castPos, new Vector2 (i, 0), moveSpeed * 2, 1 << LayerMask.NameToLayer ("RaycastThis"));
				if (hitA.collider != null) {
					if (hitA.collider.GetComponent<SpaceCheck> ().hasPiece && hitA.collider.GetComponent<SpaceCheck> ().pieceColor != PieceColor) { // check if has piece AND if has an enemy... if yes, state = canTake
						hitA.collider.GetComponent<SpaceCheck> ().myState = SpaceCheck.SpaceState.Can_Take; // change state of tile to canTake
					} else if (!hitA.collider.GetComponent<SpaceCheck> ().hasPiece) {
						hitA.collider.GetComponent<SpaceCheck> ().myState = SpaceCheck.SpaceState.Can_Move; // change state of tile to canMove
					}
				}
				//Debug.DrawRay(new Vector3(castPos.x, castPos.y, -2), new Vector3(i, 0, -2), Color.blue, 4);

				RaycastHit2D hitB = Physics2D.Raycast (castPos, new Vector2 (0, j), moveSpeed * 2, 1 << LayerMask.NameToLayer ("RaycastThis"));
				if (hitB.collider != null) {
					if (hitB.collider.GetComponent<SpaceCheck> ().hasPiece && hitB.collider.GetComponent<SpaceCheck> ().pieceColor != PieceColor) { // check if has piece AND if has an enemy... if yes, state = canTake
						hitB.collider.GetComponent<SpaceCheck> ().myState = SpaceCheck.SpaceState.Can_Take; // change state of tile to canTake
					} else if (!hitB.collider.GetComponent<SpaceCheck> ().hasPiece) {
						hitB.collider.GetComponent<SpaceCheck> ().myState = SpaceCheck.SpaceState.Can_Move; // change state of tile to canMove
					}
				}
				//Debug.DrawRay(new Vector3(castPos.x, castPos.y, -2), new Vector3(0, j, -2), Color.red, 4);
			}
		}
	}

	void CheckRoque(GameManager.Types pieceToLookFor){
		Vector2 pos = new Vector2 (myTransform.position.x, myTransform.position.y);

		if (!hasMovedBefore) {

			for (int i = -1; i < 2; i += 2) {

				RaycastHit2D[] hit = Physics2D.RaycastAll (pos, new Vector2 (i,0), 16, 1 << LayerMask.NameToLayer ("RaycastThis"));

				for (int a = 0; a < hit.Length; a++) {

					if (hit [a].collider != null) {

						if (hit [a].collider.GetComponent<SpaceCheck> ().pieceInHere != null && hit [a].collider.GetComponent<SpaceCheck> ().pieceInHere.GetComponent<MasterClass> ().PieceType != pieceToLookFor) {
							break;

						} else if (hit [a].collider.GetComponent<SpaceCheck> ().pieceColor == PieceColor && hit [a].collider.GetComponent<SpaceCheck> ().pieceInHere.GetComponent<MasterClass> ().PieceType == pieceToLookFor && !hit [a].collider.GetComponent<SpaceCheck> ().pieceInHere.GetComponent<MasterClass> ().hasMovedBefore) {

							hit [a].collider.GetComponent<SpaceCheck> ().myState = SpaceCheck.SpaceState.Can_Special;
				
						}
					}
				}
			}
		}
	}

	private void OnDestroy()
	{
		if (PieceType == GameManager.Types.King)
		{
			if (PieceColor == GameManager.Colors.White) //Negro gana
			{
				Debug.Log("Black wins!");
				GameManager.instance.textWin.text = "Black Wins";
				GameManager.instance.PieceGroups [0].position -= new Vector3 (0, 0, 0.8f);	//Negras arriba del espacio
				GameManager.instance.PieceGroups [1].position -= new Vector3 (0, 0, 0.8f);
			}
			else if (PieceColor == GameManager.Colors.Black) //Blacno gana
			{
				Debug.Log("White wins!");
				GameManager.instance.textWin.text = "White Wins";
				GameManager.instance.PieceGroups [0].position -= new Vector3 (0, 0, 0.8f);	//Negras arriba del espacio
				GameManager.instance.PieceGroups [1].position -= new Vector3 (0, 0, 0.8f);
			}
		}
	}
}