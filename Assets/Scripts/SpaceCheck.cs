﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpaceCheck : MonoBehaviour {
	//Variables privadas
	private Transform myTransform;

	//Variables públicas
	public bool hasPiece = false;
	public GameObject pieceInHere;
	public GameManager.Colors pieceColor = 0;	//es la misma que enum
	public SpaceState myState= SpaceState.Null;	//Para decidir de color colorear
	public Color[] myColors;	//Array de colores para cambiar el color del espacio
	public Renderer rend;		//Para cambiar color

	// Use this for initialization
	void Start () {
		myTransform = transform;

	}

	void Update () {
		rend.material.color = myColors [(int)myState];		//Cambiar color del material al del espacio del valor de myState

		if (GameManager.instance.PieceSelected == 0) {		//Si no hay pieza seleccionada
			myState = SpaceState.Null;						//Cambiar a Null, no hay nada
		}
		if (myState == SpaceState.Can_Special)
		{
			myTransform.position = new Vector3(myTransform.position.x, myTransform.position.y, -1.2f);
		}
		else
		{
			myTransform.position = new Vector3(myTransform.position.x, myTransform.position.y, -0.3f);
		}
		
	}
	
	void OnTriggerEnter2D(Collider2D other){	//Cuando una pieza esta en el tile hasPiece se vuelve True
		if (other.tag == "Piece") {
			
			hasPiece = true;
			pieceInHere = other.gameObject;	//agregar pieza obbjeto a la variable
		}
		pieceColor = (other.GetComponent<MasterClass> ().PieceColor);
	//Debug.Log ("Checado");// Comvertir variable color 
	}

	void OnTriggerExit2D(Collider2D other){		//Cuando una pieza abandona el tile se vuelve falso hasPiece
		if (other.tag == "Piece") {
			
			hasPiece = false; 		//Esta vacia
			pieceInHere = null;		//Remover 
		}
		pieceColor = 0;	//Remueve el color porque no hay pieza
		myState = SpaceState.Null;	//No hay nada
	}

	void OnMouseDown(){
		if (myState == SpaceState.Can_Move) {
			
			GameManager.instance.MoveItTakeIt (new Vector2 (myTransform.position.x, myTransform.position.y));

		} else if (myState == SpaceState.Can_Take) {
			GameManager.instance.MoveItTakeIt (new Vector2 (myTransform.position.x, myTransform.position.y), 1, pieceInHere);
		} else if (myState == SpaceState.Can_Special) {
			GameManager.instance.MoveItTakeIt (new Vector2 (myTransform.position.x, myTransform.position.y), 2, pieceInHere);
		}
	}

	public enum SpaceState {
		Null = 0,		//No puede tomar ni mover
		Can_Move,	//Vacio, se puede mover
		Can_Take,	//Enemy, puede comerlo
		Can_Special
	}

}
