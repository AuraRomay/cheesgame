﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {
	
	[HideInInspector]

	public static GameManager instance;  //static permite que sea usado en cualquier lugar

	public Colors Turn;		//De quien es el turno
	public Types PieceSelected; 
	public GameObject MovingPiece;	//Tener el objeto que esta seleccionado
	public Transform[] PieceGroups;	//Cambiar profundidad
	public Text textWin;

	public bool Win = false;


	[Header("Visual Feedback")]
	public Vector3 selectedScale;
	public Color selectedColor;


	void Awake() {
		instance = this; 
	}

	void Start(){
		//Hace la Z del grupo = -0.2 para darle clic al espacio en vez de la pieza. Pero sólo cuando no es mi turno
		PieceGroups[1].position += new Vector3(0,0,0.8f);
		textWin.text = "";

	}

	public void MoveItTakeIt(Vector2 where, int action = 0, GameObject other=null){	// 0= muevo, 1= como, 2= switch
		if (action == 1) {
			DestroyObject (other);
		}else if(action == 2){
			other.transform.position = MovingPiece.transform.position;
		}
			
		MovingPiece.transform.position= new Vector3(where.x, where.y, -1);
		MovingPiece.GetComponent<MasterClass> ().hasMovedBefore = true;
		ClearSelection ();


		//Cambia el turno
		if (Turn == Colors.White) {				//Turno de las negras, hazlo blancas

			Turn = Colors.Black;
			PieceGroups [0].position -= new Vector3 (0, 0, 0.8f);	//Negras arriba del espacio
			PieceGroups [1].position += new Vector3 (0, 0, 0.8f);	//Blancas arriba del espacio

		} else if (Turn == Colors.Black) {		//Turno de las negras, hazlo blancas
			
			Turn = Colors.White;
			PieceGroups [0].position += new Vector3 (0, 0, 0.8f);	//Negras arriba del espacio
			PieceGroups [1].position -= new Vector3 (0, 0, 0.8f);	//Blancas arriba del espacio

		}
	}
	public void ClearSelection(){
		//Cambia variables y scripts
		MovingPiece.GetComponent<MasterClass> ().isSelected = false;
		MovingPiece.GetComponent<MasterClass> ().hasCasted = false;
		MovingPiece.GetComponent<MasterClass> ().transform.localScale -= selectedScale;
		MovingPiece.GetComponent<MasterClass> ().rend.material.color = Color.white;

		//cambia las variables de los objetos
		PieceSelected = 0;
		MovingPiece = null;
	}
		 

	public enum Types{ 		//Les asigna un número automáticamente
		None = 0,
		Pawn,
		Tower,
		Knight,
		Bishop,
		Queen,
		King
	}

	public enum Colors{
		None = 0,
		Black,
		White
	}
}
